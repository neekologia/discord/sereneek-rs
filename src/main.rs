use std::env;
use std::io::Write;
use std::net::Shutdown;
use std::os::unix::net::UnixStream;

use serenity::all::ChannelId;
use serenity::all::VoiceState;
use serenity::async_trait;
use serenity::model::gateway::Ready;
use serenity::prelude::*;

struct Handler;

const CHANNEL: ChannelId = ChannelId::new(913477740970258454);
const SOCKET: &str = "/tmp/dis-rs.sock";

#[async_trait]
impl EventHandler for Handler {
	async fn voice_state_update(&self, ctx: Context, old: Option<VoiceState>, new: VoiceState) {
		let (joined, voice_state) = match new.channel_id {
			Some(_) => match old {
				Some(_) => return,
				None => (true, new),
			},
			None => match old {
				Some(old) => (false, old),
				None => return,
			},
		};

		let Some(channel_id) = voice_state.channel_id else {
			return;
		};

		if channel_id.get() != CHANNEL.get() {
			return;
		}

		let Ok(channel_name) = channel_id.name(ctx).await else {
			return;
		};

		let Some(ref member) = voice_state.member else {
			return;
		};

		let m = if joined {
			format!("(discord) {} joined {}", member.user.name, channel_name)
		} else {
			format!("(discord) {} left {}", member.user.name, channel_name)
		};

		let mut stream = match UnixStream::connect(SOCKET) {
			Ok(stream) => stream,
			Err(err) => {
				eprintln!("{:?}", err);
				return;
			}
		};
		stream.write(m.as_bytes()).unwrap();
		stream.shutdown(Shutdown::Both).unwrap();
	}

	async fn ready(&self, _: Context, ready: Ready) {
		println!("{} is connected!", ready.user.name);
	}
}

#[tokio::main]
async fn main() {
	let token = env::var("DISCORD_TOKEN").expect("Expected a token in the environment");

	let intents = GatewayIntents::GUILD_VOICE_STATES | GatewayIntents::GUILDS;
	let mut client = Client::builder(&token, intents)
		.cache_settings(serenity::cache::Settings::default())
		.event_handler(Handler)
		.await
		.expect("Err creating client");

	if let Err(why) = client.start().await {
		println!("Client error: {why:?}");
	}
}
